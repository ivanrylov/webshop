﻿using System;
using WebShop.Data.Domain;
using WebShop.Data.Repositories;

namespace WebShop.Data
{
    /// <summary>
    /// Implementetion of unitofwork pattern.
    /// </summary>
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private Repository<Customer> _customerRepository;
        private Repository<Article> _articleRepository;
        private Repository<Order> _orderRepository;
        private readonly IContext _dbContext;

        public UnitOfWork(IContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        public void Commit()
        {
            _dbContext.Save();
        }

        public Repository<Customer> CustomerRepository
        {
            get
            {
                if(_customerRepository == null)
                    _customerRepository = new Repository<Customer>(_dbContext);
                return _customerRepository;
            }
        }

        public Repository<Article> ArticleRepository
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new Repository<Article>(_dbContext);
                return _articleRepository;
            }
        }

        public Repository<Order> OrderRepository
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new Repository<Order>(_dbContext);
                return _orderRepository;
            }
        }
        
    }
}
