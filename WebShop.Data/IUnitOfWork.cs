﻿using WebShop.Data.Domain;
using WebShop.Data.Repositories;

namespace WebShop.Data
{
    /// <summary>
    /// Interface for UnitofWork.
    /// </summary>
    public interface IUnitOfWork
    {
        void Commit();
        Repository<Customer> CustomerRepository { get; }
        Repository<Article> ArticleRepository { get; }
        Repository<Order> OrderRepository { get; }
    }
}
