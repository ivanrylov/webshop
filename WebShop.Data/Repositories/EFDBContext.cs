﻿using System.Data.Entity;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// DbContext for managing database through Entity Framework.
    /// </summary>
    public class EFDBContext : DbContext, IContext
    {
        public EFDBContext()
            : base("EFDBContext")
        {}

        public DbSet<Domain.Article> Articles { set; get; }
        public DbSet<Domain.Customer> Customers { set; get; }
        public DbSet<Domain.Order> Orders { set; get; }


        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        public void Save()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DBInitializer());

            base.OnModelCreating(modelBuilder);
        }
    }
}
