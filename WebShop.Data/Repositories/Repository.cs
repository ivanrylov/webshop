﻿using System.Linq;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// Generic repository class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : class
    {
        protected IContext Context;

        public Repository()
        {
            Context = new EFDBContext();
        }

        public Repository(IContext dataContext)
        {
            Context = dataContext;
        }

        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public void Add(T obj)
        {
            Context.Set<T>().Add(obj);
        }

        public void Save()
        {
            Context.Save();
        }
    }
}
