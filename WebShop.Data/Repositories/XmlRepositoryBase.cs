﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using WebShop.Common;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// Generic repository class for managing xml data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class XmlRepositoryBase<T> : IPageable<T>, IRepository<T> where T : class, new()
    {
        private string _xmlKeyNodeName;
        private string _file;
        public XmlRepositoryBase(string file, string xmlKeyNodeName)
        {
            _file = file;
            _xmlKeyNodeName = xmlKeyNodeName.ToLower();
        }

        public virtual XmlReader GetReader()
        {
            return XmlReader.Create(_file);
        }

        private T ReadXml(int id)
        {
            using (XmlReader source = GetReader())
            {
                while (source.Read())
                {
                    if (source.NodeType == XmlNodeType.Element && source.Name == _xmlKeyNodeName && source.GetAttribute("id") == id.ToString())
                    {
                        return CustomMapper<T>.Map(source);
                    }
                }
                return null;
            }
        }

        private IQueryable<T> ReadXml()
        {
            using (XmlReader source = GetReader())
            {
                List<T> result = new List<T>();
                while (source.Read())
                {
                    if (source.NodeType == XmlNodeType.Element && source.Name == _xmlKeyNodeName)
                    {
                        result.Add(CustomMapper<T>.Map(source));
                    }
                }
                return result.AsQueryable();
            }
        }

        private IQueryable<T> ReadXml(int pageSize, int pageIndex)
        {
            using (XmlReader source = GetReader())
            {
                int count = 0;
                List<T> result = new List<T>(pageSize);
                while (source.Read())
                {
                    if (source.NodeType == XmlNodeType.Element && source.Name.ToLower() == _xmlKeyNodeName)
                    {
                        count++;
                        if (count > pageIndex * pageSize)
                        {
                            result.Add(CustomMapper<T>.Map(source));
                            if (result.Count == pageSize)
                                break;
                        }
                    }
                }
                return result.AsQueryable();
            }
        }

        public IQueryable<T> GetAll()
        {
            return ReadXml();
        }

        public int TotalCount
        {
            get
            {
                using (XmlReader source = GetReader())
                {
                    int count = 0;
                    while (source.Read())
                    {
                        if (source.NodeType == XmlNodeType.Element && source.Name.ToLower() == _xmlKeyNodeName)
                            count++;
                    }
                    return count;
                }
            }
        }

        public T GetById(int id)
        {
            return ReadXml(id);
        }

        public IQueryable<T> GetAll(int pageSize, int pageIndex)
        {
            return ReadXml(pageSize, pageIndex);
        }

        public void Add(T obj)
        {
            //read only
            throw new NotImplementedException();
        }

        public void Save()
        {
            //read only
            throw new NotImplementedException();
        }
    }

}
