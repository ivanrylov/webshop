﻿namespace WebShop.Data.Repositories.Article
{
    /// <summary>
    /// Concrete ArticleXmlRepository.
    /// </summary>
    public class ArticleXmlRepository : XmlRepositoryBase<Domain.Article>
    {
        public ArticleXmlRepository(string file, string xmlKeyNodeName) : base(file, xmlKeyNodeName) { }
    }
}
