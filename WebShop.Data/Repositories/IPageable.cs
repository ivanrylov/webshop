﻿using System.Linq;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// Interface for pageable logic.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPageable<T>
    {
        IQueryable<T> GetAll(int pageSize, int pageIndex);
        int TotalCount { get; }
    }
}
