﻿using System.Linq;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// Interface for repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T obj);
        void Save();
    }
}
