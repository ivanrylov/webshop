﻿using System;
using System.Data.Entity;

namespace WebShop.Data.Repositories
{
    /// <summary>
    /// Interface for Context entity.
    /// </summary>
    public interface IContext : IDisposable
    {
        IDbSet<T> Set<T>() where T : class;
        void Save();
    }
}
