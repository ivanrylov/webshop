﻿using WebShop.Data.Repositories;

namespace WebShop.Data
{
    /// <summary>
    /// Class for initialization of database by Entity Framework.
    /// </summary>
    public class DBInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<EFDBContext>
    {
    }
}
