﻿using System.ComponentModel.DataAnnotations;

namespace WebShop.Data.Domain
{
    /// <summary>
    /// Customer domain entity.
    /// </summary>
    public class Customer
    {
        [Key]
        public int Id { set; get; }
        public string Title { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Address { set; get; }
        public string HouseNumber { set; get; }
        public int ZipCode { set; get; }
        public string City { set; get; }
        public string Email { set; get; }
    }
}
