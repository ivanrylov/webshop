﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebShop.Data.Domain
{
    /// <summary>
    /// Article domain entity.
    /// </summary>
    public class Article
    {
        [Key]
        public int Id { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; }
        [NotMapped] 
        public string Description { set; get; }
    }
}
