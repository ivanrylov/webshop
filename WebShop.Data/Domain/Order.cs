﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace WebShop.Data.Domain
{
    /// <summary>
    /// Order domain entity.
    /// </summary>
    public class Order
    {
        [Key]
        public int Id { set; get; }
        public virtual ICollection<Article> Articles { set; get; }
        public virtual Customer Customer { set; get; }
    }
}
