﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using Services.OrderService;
using WebShop.APIService.Models;
using Order = WebShop.APIService.Models.Order;

namespace WebShop.APIService.Controllers
{
    /// <summary>
    /// Controller for managing orders.
    /// </summary>
    public class OrderController : ApiController
    {
        private IOrderService _service;
        public OrderController(IOrderService service)
        {
            _service = service;
        }
        
        [HttpPost]
        [ActionName("Place")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Place([FromBody]Order order)
        {
            Services.DTO.Order DTOOrder = Mapper.Map<Order, Services.DTO.Order>(order);
            bool save = _service.Save(DTOOrder);
            if (save)
                return Request.CreateResponse(HttpStatusCode.OK, string.Empty);
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }
       
    }
}