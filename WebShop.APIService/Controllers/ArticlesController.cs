﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using Services.ArticleService;
using WebShop.APIService.Models;
using WebShop.Common;

namespace WebShop.APIService.Controllers
{
    /// <summary>
    /// Controller for managing articles.
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ArticlesController : ApiController
    {
        private IXmlArticleService _service;
        
        public ArticlesController(IXmlArticleService service)
        {
            _service = service;
        }

        public IEnumerable<Article> GetAllArticles()
        {
            List<Services.DTO.Article> articles = _service.GetAll().ToList();
            return Mapper.Map<List<Services.DTO.Article>, List<Article>>(articles);
        }

        public ArticleDetailed GetArticle(int id)
        {
            Services.DTO.Article article = _service.GetById(id);
            ArticleDetailed articleDetailed = Mapper.Map<Services.DTO.Article, ArticleDetailed>(article);
            return articleDetailed;
        }

        public ArticleContainer GetAllArticles(int pageIndex, int pageSize)
        {
            List<Services.DTO.Article> articles = _service.GetAll(pageSize, pageIndex).ToList();
            IList<Article> list = Mapper.Map<List<Services.DTO.Article>, List<Article>>(articles);
            return new ArticleContainer
            {
                Articles = list,
                TotalPagesCount = Utils.GetTotalPages(_service.TotalCount, pageSize)
            };
        }
    }
}
