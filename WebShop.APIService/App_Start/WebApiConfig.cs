﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.Hosting;
using System.Web.Http;
using AutoMapper;
using Microsoft.Practices.Unity;
using Services;
using Services.ArticleService;
using Services.OrderService;
using WebShop.APIService.Models;
using WebShop.Data.Repositories;
using WebShop.Data.Repositories.Article;

namespace WebShop.APIService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();
         
            var container = new UnityContainer();
            string filePath="";

            string articleXmlFilePath = ConfigurationManager.AppSettings["ArticleXmlFilePath"];
            string articleXmlFileKeyNodeName = ConfigurationManager.AppSettings["ArticleXmlFileKeyNodeName"];
            if (string.IsNullOrEmpty(articleXmlFilePath) || string.IsNullOrEmpty(articleXmlFileKeyNodeName))
                throw new Exception("Configuration key ArticleXmlFilePath is missing");
            filePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, articleXmlFilePath);

            container.RegisterType<IXmlArticleService, XmlArticleService>(new InjectionConstructor(filePath, articleXmlFileKeyNodeName));
            container.RegisterType<IOrderService, OrderService>(new InjectionConstructor(new EFDBContext()));
            config.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            Mapper.CreateMap<Services.DTO.Article, ArticleDetailed>();
            Mapper.CreateMap<Services.DTO.Article, Article>();
            Mapper.CreateMap<ArticleDetailed, Services.DTO.Article>();
            Mapper.CreateMap<Article, Services.DTO.Article>();
            Mapper.CreateMap<Order, Services.DTO.Order>();
            Mapper.CreateMap<Customer, Services.DTO.Customer>();
        }
    }
}
