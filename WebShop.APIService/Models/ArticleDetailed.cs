﻿using System.Runtime.Serialization;

namespace WebShop.APIService.Models
{
    /// <summary>
    /// Article with additional data.
    /// </summary>
    [DataContract]
    public class ArticleDetailed : Article
    {
        [DataMember]
        public string Description { set; get; }
    }
}