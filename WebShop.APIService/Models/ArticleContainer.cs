﻿using System.Collections.Generic;

namespace WebShop.APIService.Models
{
    /// <summary>
    /// Container model for Articles paging logic.
    /// </summary>
    public class ArticleContainer
    {
        public IEnumerable<Article> Articles { set; get; }
        public int TotalPagesCount { set; get; }
    }
}