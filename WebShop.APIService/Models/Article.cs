﻿using System.Runtime.Serialization;

namespace WebShop.APIService.Models
{
    /// <summary>
    /// Article model.
    /// </summary>
    [DataContract]
    public class Article
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public decimal Price { set; get; }
    }
}