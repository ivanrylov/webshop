﻿using System.Collections.Generic;

namespace WebShop.APIService.Models
{
    /// <summary>
    /// Order model.
    /// </summary>
    public class Order
    {
        public virtual IEnumerable<Article> Articles { set; get; }
        public virtual Customer Customer { set; get; }
    }
}