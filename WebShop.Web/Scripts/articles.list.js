﻿function formatCurrency(value) {
    return "$" + value.toFixed(2);
}

function isElementExists(array, obj) {
    var result = false;
    $.each(array, function (index, item) {
        if (item.Id === obj.Id)
            result = true;
    });
    return result;
}
    
var articlesModel = function (items, totalCount, cartItems) {
    var self = this;
        
    self.openArticle = function openDialog(id) {
        $.getJSON(pageConfig.articlesUrl + "/" + id)
            .done(function (data) {
                var myModal = new jBox('Modal').setTitle(data.Name + " <b>$" + data.Price + "</b>").setContent('<div class="article-details">' + data.Description + '<input type="button" value="Add to cart" onclick="AddToCart(' + id + ')" /> </div>').open();
            });
    };

    self.items = ko.observableArray(items);
    self.currentPageIndex = ko.observable(0);
    self.nextPageIndex = ko.observable(0);
    self.pageSize = pageConfig.articlesPageSize;
    self.totalPagesCount = totalCount;

    self.allPages = ko.computed(function () {
        var pages = [];
        for (var i = 0; i < self.totalPagesCount ; i++) {
            pages.push({ pageNumber: (i + 1) });
        }
        return pages;
    }, this);

    self.moveToPage = function (index) {
        self.nextPageIndex = index;
        self.items.currentPageIndex = ko.observable(index);
        $.getJSON(pageConfig.articlesUrl + "?pagesize=" + self.pageSize + "&pageindex=" + index)
            .done(function (data) {
                self.items(data.Articles);
                self.totalPagesCount = data.TotalPagesCount;
            });
    };

    self.addedToCart = ko.observableArray(cartItems);
    self.addToCart = function (item) {
        if (!isElementExists(self.addedToCart(), item)) {
            self.addedToCart.push(item);
            localStorage.setItem('cart', JSON.stringify(self.addedToCart()));
        }
    };

};
    
function AddToCart(id) {
    $("#buyBtn" + id).click();
}

$(document).ready(function () {
    $(".articles").hide();
    $(".loading").show();
    $.getJSON(pageConfig.articlesUrl + "?pagesize=" + pageConfig.articlesPageSize + "&pageindex=0")
        .done(function (data) {
            var cartItems = [];
            if (localStorage.getItem('cart') != undefined && localStorage.getItem('cart').length!=0)
                cartItems = JSON.parse(localStorage.getItem('cart'));
           
            ko.applyBindings(new articlesModel(data.Articles, data.TotalPagesCount, cartItems));
            $(".loading").hide();
            $(".articles").show();
        });

});
