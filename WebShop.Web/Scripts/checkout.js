﻿$(".checkout-form").submit(function () {
    if (!$(this).valid())
        return false;

    $(".loading").show();

    var formData = {};
    var data = {};
    $(".checkout-form").serializeArray().map(function (x) { formData[x.name] = x.value; });
    data = { "customer": formData };
    data["articles"] = JSON.parse(localStorage.getItem('cart'));
    data = JSON.stringify(data);

    $.ajax({
        type: "POST",
        url: pageConfig.orderUrl,
        data: data,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function () {
            $(".checkout-block").hide();
            $(".cart-form").hide();
            $(".message-error").hide();
            $(".message-success").show();
            localStorage.removeItem('cart');
        },
        error: function () {
            $(".message-error").show();
        }
    });

    return false;
});
