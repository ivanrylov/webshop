﻿
function formatCurrency(value) {
    return "$" + value.toFixed(2);
}

var cartModel = function (items,vat) {
    var self = this;
    self.items = ko.observableArray(items);
    self.VAT = parseFloat(vat);

    self.subtotal = ko.computed(function () {
        var subtotal = 0;
        $.each(self.items(), function (index, item) {
            subtotal += parseFloat(item.Price);
        });

        return parseFloat(subtotal);
    });
    self.totalVAT = ko.computed(function () {
        return parseFloat(self.subtotal()) * parseFloat(self.VAT);
    });
    self.totalWithVAT = ko.computed(function () {
        return parseFloat(self.subtotal()) + parseFloat(self.subtotal()) * parseFloat(self.VAT);
    });

    self.removeItem = function (item) {
        self.items.remove(item);
        localStorage.setItem('cart', JSON.stringify(self.items()));
        if (self.items().length == 0) {
            showEmptyMessage();
        }
    };
};


$(document).ready(function () {
    var cartItems = [];
    if (localStorage.getItem('cart') != undefined) {
        cartItems = JSON.parse(localStorage.getItem('cart'));
        if (cartItems.length != 0) {
            ko.applyBindings(new cartModel(cartItems, pageConfig.VAT));
        }
        else {
            showEmptyMessage();
        }
    } else {
        showEmptyMessage();
    }
});

function showEmptyMessage() {
    $(".checkout-block").hide();
    $(".cart-form").hide();
    $(".message-empty").show();
}
