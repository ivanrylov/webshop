﻿using System.Collections.Generic;

namespace Services.ArticleService
{
    /// <summary>
    /// Interface for XmlArticeService.
    /// </summary>
    public interface IXmlArticleService : IPageable<DTO.Article>
    {
        IEnumerable<DTO.Article> GetAll();
        DTO.Article GetById(int id);
    }
}
