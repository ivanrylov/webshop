﻿using System.Collections.Generic;

namespace Services.ArticleService
{
    /// <summary>
    /// Interface for managing paging logic.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPageable<T>
    {
        IEnumerable<T> GetAll(int pageSize, int pageIndex);
        int TotalCount { get; }
    }
}
