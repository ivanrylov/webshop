﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using WebShop.Data.Domain;

namespace Services.ArticleService
{
    /// <summary>
    /// Service for managing article entities.
    /// </summary>
    public class XmlArticleService : IXmlArticleService
    {
        private WebShop.Data.Repositories.XmlRepositoryBase<Article> _repository;

        public XmlArticleService(string fileName, string keyNodeName)
        {
            _repository = new WebShop.Data.Repositories.Article.ArticleXmlRepository(fileName, keyNodeName);
        }

        public IEnumerable<DTO.Article> GetAll()
        {
            Mapper.CreateMap<Article, DTO.Article>();
            List<Article> articles = _repository.GetAll().ToList();
            List<DTO.Article> list = Mapper.Map<List<Article>, List<DTO.Article>>(articles);
            return list.AsEnumerable(); 
        }

        public IEnumerable<DTO.Article> GetAll(int pageSize, int pageIndex)
        {
            Mapper.CreateMap<Article, DTO.Article>();
            List<Article> articles = _repository.GetAll(pageSize, pageIndex).ToList();
            List<DTO.Article> list = Mapper.Map<List<Article>, List<DTO.Article>>(articles);
            return list.AsEnumerable();
        }

        public int TotalCount 
        {
            get { return _repository.TotalCount; }
        }

        public DTO.Article GetById(int id)
        {
            Mapper.CreateMap<Article, DTO.Article>();
            Article article = _repository.GetById(id);
            return Mapper.Map<Article, DTO.Article>(article);
        }
    }
}
