﻿namespace Services.OrderService
{
    /// <summary>
    /// Interface for Order service
    /// </summary>
    public interface IOrderService
    {
        bool Save(DTO.Order order);
    }
}
