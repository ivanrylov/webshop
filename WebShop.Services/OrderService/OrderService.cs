﻿using System;
using AutoMapper;
using WebShop.Data;
using WebShop.Data.Domain;
using WebShop.Data.Repositories;

namespace Services.OrderService
{
    /// <summary>
    /// Order service class for managing Order entity
    /// </summary>
    public class OrderService : IOrderService
    {
        private IUnitOfWork _unitofwork;

        public OrderService(IContext context)
        {
            _unitofwork = new UnitOfWork(context);
        }

        /// <summary>
        /// Save order entity with child elements
        /// </summary>
        /// <param name="tdoOrder">Order entity</param>
        /// <returns>Boolean</returns>
        public bool Save(DTO.Order tdoOrder)
        {
            try
            {
                Mapper.CreateMap<DTO.Order, Order>();
                Mapper.CreateMap<DTO.Article, Article>();
                Mapper.CreateMap<DTO.Customer, Customer>();
                Order order = Mapper.Map<DTO.Order, Order>(tdoOrder);
                _unitofwork.OrderRepository.Add(order);
                _unitofwork.Commit();
                return true;
            }
            catch (Exception ex)
            {
                //loging exception
                return false;
            }
        }


    }
}
