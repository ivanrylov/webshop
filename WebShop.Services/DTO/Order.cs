﻿using System.Collections.Generic;

namespace Services.DTO
{
    /// <summary>
    /// Order entity.
    /// </summary>
    public class Order
    {
        public int Id { set; get; }
        public IList<Article> Articles { set; get; }
        public Customer Customer { set; get; }
    }
}
