﻿namespace Services.DTO
{
    /// <summary>
    /// Article entity.
    /// </summary>
    public class Article
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; }
        public string Description { set; get; }
    }
}
