﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using NUnit.Framework;
using WebShop.Data.Domain;
using WebShop.Data.Repositories;

namespace WebShop.Data.Tests.IntegrationTests
{
    /// <summary>
    /// Integration tests for OrderRepository.
    /// </summary>
    [TestFixture]
    public class OrderRepositoryTest
    {
        private TransactionScope _transactionScope;
        [SetUp]
        public void TestSetup()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ""));
            EFDBContext context = new EFDBContext();
            context.Database.CreateIfNotExists();
            _transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew);
        }

        [TearDown]
        public void TestCleanUp()
        {
            _transactionScope.Dispose();
        }

        [Test]
        public void OrderRepository_SaveOrder_ReturnsTrue()
        {
            Order order1 = new Order();
            order1.Articles = new List<Article>(10);
            for (int i = 0; i < 10; i++)
            {
                Article article = new Article();
                article.Name = "test";
                article.Price = 10;
                order1.Articles.Add(article);
            }

            Customer customer = new Customer();
            customer.Address = "Test street";
            customer.City = "London";
            customer.Email = "test@email.com";
            customer.FirstName = "Test";
            customer.HouseNumber = "10b";
            customer.LastName = "asdf";
            customer.Title = "asdf";
            customer.ZipCode = 10212;

            order1.Customer = customer;

            Repository<Order> _repository = new Repository<Order>(new EFDBContext());
            _repository.Add(order1);
            _repository.Save();

            IQueryable<Order> queryable = _repository.GetAll().Where(s => s.Customer.ZipCode == customer.ZipCode && s.Customer.Email == customer.Email);

            Assert.IsTrue(queryable.Count() == 1);
            Assert.IsTrue(queryable.First().Articles.Count == order1.Articles.Count);
            Assert.IsTrue(queryable.First().Articles.Sum(s => s.Price) == order1.Articles.Sum(s => s.Price));
            Assert.IsTrue(queryable.First().Customer.HouseNumber == customer.HouseNumber);
            Assert.IsTrue(queryable.First().Customer.Address == customer.Address);
            Assert.IsTrue(queryable.First().Customer.City == customer.City);
            Assert.IsTrue(queryable.First().Customer.Email == customer.Email);
            Assert.IsTrue(queryable.First().Customer.FirstName == customer.FirstName);
            Assert.IsTrue(queryable.First().Customer.LastName == customer.LastName);
            Assert.IsTrue(queryable.First().Customer.Title == customer.Title);
        }
    }

}
