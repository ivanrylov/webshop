﻿using System.Linq;
using NUnit.Framework;
using WebShop.Common;
using WebShop.Data.Domain;
using WebShop.Data.Repositories.Article;

namespace WebShop.Data.Tests.UnitTests
{
    /// <summary>
    /// Unit tests for XmlRepository.
    /// </summary>
    [TestFixture]
    public class XMLRepositoryTests
    {
        private ArticleXmlRepository _repository;
        private int _totalTestCount;

        [SetUp]
        public void SetUp()
        {
            string data = "<articles><article name=\"artcle\" id=\"1\" description=\"Longtime Figure main-player\" price=\"7.99\"></article><article name=\"Len Fakis\" id=\"2\" description=\"Superb DEBUT album on Len Fakis most reliable Figure imprint!\" price=\"14.99\"></article><article name=\"artcle\" id=\"3\" description=\"Longtime Figure main-player\" price=\"7.99\"></article><article name=\"Len Fakis\" id=\"4\" description=\"Superb DEBUT album on Len Fakis most reliable Figure imprint!\" price=\"14.99\"></article><article name=\"artcle\" id=\"5\" description=\"Longtime Figure main-player\" price=\"7.99\"></article><article name=\"Len Fakis\" id=\"6\" description=\"Superb DEBUT album on Len Fakis most reliable Figure imprint!\" price=\"14.99\"></article><article name=\"artcle\" id=\"7\" description=\"Longtime Figure main-player\" price=\"7.99\"></article><article name=\"Len Fakis\" id=\"8\" description=\"Superb DEBUT album on Len Fakis most reliable Figure imprint!\" price=\"14.99\"></article></articles>";
            _totalTestCount = 8;
            _repository = new ArticleXmlRepositoryTest("test", "article", data);
        }


        [Test]
        public void ArticleXmlRepository_GetById_ReturnsElement()
        {
            int testId = 2;
            Article byId = _repository.GetById(testId);
            Assert.IsTrue(byId.Id == testId);
        }

        [Test]
        public void ArticleXmlRepository_GetById_ReturnsNull()
        {
            int testId = 10;
            Article byId = _repository.GetById(testId);
            Assert.IsTrue(byId==null);
        }

        [Test]
        public void ArticleXmlRepository_GetAll_ReturnsCollection()
        {
            IQueryable<Article> articles = _repository.GetAll();
            Assert.IsTrue(articles.Count() == _totalTestCount);
            Assert.IsTrue(_repository.TotalCount == _totalTestCount);
            foreach (Article article in articles)
            {
                Assert.IsTrue(article != null);
                Assert.IsTrue(article.Id != 0);
                Assert.IsTrue(article.Name != null);
                Assert.IsTrue(article.Price != 0);
                Assert.IsTrue(article.Description != null);
            }
        }

        [Test]
        public void ArticleXmlRepository_GetAllPaging_ReturnsCollection()
        {
            int pageSize = 5;
            int pageIndex = 0;
            IQueryable<Article> articles = _repository.GetAll(pageSize, pageIndex);
            Assert.IsTrue(_repository.TotalCount == _totalTestCount);
            Assert.IsTrue(articles.Count() <= pageSize);
            Assert.IsTrue(articles.Count() != 0);

            pageSize = 5;
            pageIndex = 1;
            articles = _repository.GetAll(pageSize, pageIndex);
            Assert.IsTrue(_repository.TotalCount == _totalTestCount);
            Assert.IsTrue(articles.Count() <= pageSize);
            Assert.IsTrue(articles.Count() != 0);

            int totalPages = Utils.GetTotalPages(_totalTestCount, pageSize);

            pageSize = 5;
            pageIndex = totalPages - 1;
            articles = _repository.GetAll(pageSize, pageIndex);
            Assert.IsTrue(_repository.TotalCount == _totalTestCount);
            Assert.IsTrue(articles.Count() <= pageSize);
            Assert.IsTrue(articles.Count() != 0);

            pageSize = 5;
            pageIndex = totalPages + 10;
            articles = _repository.GetAll(pageSize, pageIndex);
            Assert.IsTrue(_repository.TotalCount == _totalTestCount);
            Assert.IsTrue(articles.Count() == 0);
        }

    }
}
