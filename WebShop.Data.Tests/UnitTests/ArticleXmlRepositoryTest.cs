﻿using System.IO;
using System.Xml;
using WebShop.Data.Repositories.Article;

namespace WebShop.Data.Tests.UnitTests
{
    /// <summary>
    /// Helper class for stubbing ArticleXmlRepository
    /// </summary>
    public class ArticleXmlRepositoryTest : ArticleXmlRepository
    {
        private string _testdata;
        public ArticleXmlRepositoryTest(string file, string xmlKeyNodeName, string testData) : base(file, xmlKeyNodeName)
        {
            _testdata = testData;
        }

        public override XmlReader GetReader()
        {
            return XmlReader.Create(new StringReader(_testdata));
        }
    }
}
