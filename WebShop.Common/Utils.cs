﻿namespace WebShop.Common
{
    /// <summary>
    /// Class for help methods.
    /// </summary>
    public class Utils
    {
        public static int GetTotalPages(int totalCount, int pageSize)
        {
            int result = 0;
            float tmp = totalCount / pageSize;
            if ((totalCount / pageSize) % pageSize != 0)
                result = (int)tmp + 1;
            else
                result = (int)tmp;
            return result;
        }
    }
}
