﻿using System;
using System.Reflection;
using System.Xml;

namespace WebShop.Common
{
    /// <summary>
    /// Class for mapping XmlReader attributes to entity.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CustomMapper<T>
    {
        public static T Map(XmlReader reader)
        {
            T obj = (T) Activator.CreateInstance(typeof (T));
            if (reader.HasAttributes)
            {
                for (int i = 0; i < reader.AttributeCount; i++)
                {
                    reader.MoveToAttribute(i);
                    PropertyInfo propertyInfo = obj.GetType().GetProperty(reader.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty | BindingFlags.IgnoreCase);
                    propertyInfo.SetValue(obj, Convert.ChangeType(reader.Value, propertyInfo.PropertyType), null);
                }
                reader.MoveToElement(); 
            }
            return obj;
        }
    }
}
